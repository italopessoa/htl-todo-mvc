/*
 * Copyright (c) 2014 Adobe Systems Incorporated. All rights reserved.
 *  
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Please note that some portions of this project are written by third parties
 * under different license terms. Your use of those portions are governed by
 * the license terms contained in the corresponding files. 
 */

var ScriptEventHandler = (function($){
	var addRemoveEvent = function(){
		//https://github.com/Dogfalo/materialize/issues/803 
		var ev = new $.Event('remove'),
		orig = $.fn.remove;
		$.fn.remove = function() {
			$(this).trigger(ev);
			return orig.apply(this, arguments);
		}
	};
	function dismissableOptionEventClick(){
		
	}
	function initializeChips(){
   	 	$('.chips-autocomplete').material_chip({
		    secondaryPlaceholder: 'Priority',
		    autocompleteOptions: {
		      data: {
		        'Trivial': null,
		        'Major': null,
		        'Blocker': null
		      },
		      limit: 1,
		      minLength: 1
		    }
		  });
	}
	
	var setDismissableClick = function() {
		//console.log('DismissableClick');
	    var viewportHalfSize = 0;
	    var touchIsGoingRight = false;
	    var touchIsGoingLeft = false;
	    var touchLastX = 0;
	    viewportHalfSize = $(window).width() / 2;

	    //https://github.com/Dogfalo/materialize/issues/803
	    $('.dismissable').unbind('remove');
	    $('.collection-item').unbind('touchmove');
	    
	    $('.dismissable').bind('remove', function() {
	        console.log('removed!');
	        var element = $(this)[0];
	        //TODO: modificar caso os itens ou a posicao deles seja alterada
	        var elementChild = touchIsGoingRight ? element.lastElementChild : element.children[2];//.firstElementChild;
	        $(elementChild).click();
	        // do pre-mortem stuff here
	        // 'this' is still a reference to the element, before removing it
	    });

	    $('.collection-item').bind({
	        touchmove: function(e) {
	            var currentX = e.originalEvent.touches[0].clientX;
	            if (currentX > touchLastX) {
	                touchIsGoingRight = true;
	                touchIsGoingLeft = !touchIsGoingRight;
	            }
	            if (currentX < touchLastX) {
	                touchIsGoingRight = false;
	                touchIsGoingLeft = !touchIsGoingRight;
	            }
	            touchLastX = currentX;
	        }
	    });
	};
	
	/**
     * Apply materialize Dismissable event
     */
    var setDismissable = function(context) {
        // Touch Event
        var swipeLeft = false;
        var swipeRight = false;

        // Dismissible Collections
        $('.dismissable', context).each(function() {
            $(this).hammer({
                prevent_default: false
            }).bind('pan', function(e) {
                if (e.gesture.pointerType === "touch") {
                    var $this = $(this);
                    var direction = e.gesture.direction;
                    var x = e.gesture.deltaX;
                    var velocityX = e.gesture.velocityX;
                	//console.log("Angle: " + e.gesture.angle);
                    $this.velocity({
                        translateX: x
                    }, {
                        duration: 50,
                        queue: false,
                        easing: 'easeOutQuad'
                    });

                    // Swipe Left
                    if (direction === 4 && (x > ($this.innerWidth() / 2) || velocityX < -0.75)) {
                        swipeLeft = true;
                    }

                    // Swipe Right
                    if (direction === 2 && (x < (-1 * $this.innerWidth() / 2) || velocityX > 0.75)) {
                        swipeRight = true;
                    }
                }
            }).bind('panend', function(e) {
                // Reset if collection is moved back into original position
                if (Math.abs(e.gesture.deltaX) < ($(this).innerWidth() / 2)) {
                    swipeRight = false;
                    swipeLeft = false;
                }

                if (e.gesture.pointerType === "touch") {
                    var $this = $(this);
                    if (swipeLeft || swipeRight) {
                        var fullWidth;
                        if (swipeLeft) {
                            fullWidth = $this.innerWidth();
                        } else {
                            fullWidth = -1 * $this.innerWidth();
                        }

                        $this.velocity({
                            translateX: fullWidth,
                        }, {
                            duration: 100,
                            queue: false,
                            easing: 'easeOutQuad',
                            complete: function() {
                                $this.css('border', 'none');
                                $this.velocity({
                                    height: 0,
                                    padding: 0,
                                }, {
                                    duration: 200,
                                    queue: false,
                                    easing: 'easeOutQuad',
                                    complete: function() {
                                        $this.remove();
                                    }
                                });
                            }
                        });
                    } else {
                        $this.velocity({
                            translateX: 0,
                        }, {
                            duration: 100,
                            queue: false,
                            easing: 'easeOutQuad'
                        });
                    }
                    swipeLeft = false;
                    swipeRight = false;
                }
            });

        });
    };
    function initInputTextCount(){
	    $('input#new-todo').characterCounter();
    };
    
	return {
		addRemoveEvent : addRemoveEvent,
		setDismissableClick : setDismissableClick,
		setDismissable: setDismissable,
		initializeChips : initializeChips,
		initInputTextCount : initInputTextCount
	}
}(jQuery));


(function ($) {
    'use strict';
    
    // Variables global to this script
    var todoapp = $('#todoapp');;
    var doingTask = $('#doingItems');
    var doneTask = $('#doneItems');
    var updatePath = todoapp.data('update-path');
    var updatePathDoing = doingTask.data('update-path');
    var updatePathDone = doneTask.data('update-path');
    var chipPriority = $('#chipPriorAutoComplete');
    var KEY = {
        ENTER: 13,
        ESCAPE: 27
    };
  
    /**
     * Refreshes asynchronously the content of the Todo app
     */
    function updateView() {
        todoapp.load(updatePath,function(){
        	ScriptEventHandler.setDismissable(todoapp);
        	ScriptEventHandler.setDismissableClick();
        	ScriptEventHandler.initializeChips();
        	chipPriority = $('#chipPriorAutoComplete');
        	ScriptEventHandler.initInputTextCount();
        	$('#new-todo').focus();
        });
        doingTask.load(updatePathDoing, function(){
        	ScriptEventHandler.setDismissable(doingTask);
        	ScriptEventHandler.setDismissableClick();
        	ScriptEventHandler.initializeChips();
        });
        doneTask.load(updatePathDone, function(){
        	ScriptEventHandler.setDismissable(doneTask);
        	ScriptEventHandler.setDismissableClick();
        	ScriptEventHandler.initializeChips();
        });  
    }

    /**
     * Generic function that POSTs an action to the server and subsequently updates the view.
     * Once the POST is done, it refreshes the view with an asynchronous GET request.
     * {HTMLElement} element: The element that must have an action data attribute with following JSON:
     *     {String} path: The URL to which the POST will be sent.
     *     {Object} data: The form data to submit, should be in the form of JSON key-value pairs.
     *     {String} append: If set, it defines the key to add to the POST data with the provided value.
     * {String} value: If provided, it provides the value of the key that is appended to the POST payload.
     */
    function performAction(element, value) {
        // One line of magic to retreive the right element, depending on how the function was called.
        element = (element instanceof $) ? element : $(element.target || element);

        var action = element.data('action');

        // Add the value to the payload if it was provided and if an append variable is provided in the action json.
        if (action.append && value !== undefined) {
            action.data[action.append] = value;
            var chipData = $(chipPriority).material_chip('data');
            if(chipData && chipData.length > 0){
            	action.data["priority"] = chipData[0].tag.toLowerCase();
            }
        }

        // Do the post and subsequently update the view.
        $.post(action.path, action.data).done(updateView);
    }

    /**
     * Adds a new todo item.
     */
    function addItem(event) {
        if (event.which === KEY.ENTER) {
            var input = $(this);
            var value = input.val().trim();
            console.log('addItem: ' + input + ' - ' + value)
            // Only create the item if the input value is not empty.
            if (value.length) {
            	var chipData = $(chipPriority).material_chip('data');
                if(chipData.length == 0){
                	var $toastContent = $('<span>Enter the priority!</span>');
                	Materialize.toast($toastContent, 3000,'red');
                }else{
                	performAction(input, value);
                	var $toastContent = $('<span>Task saved.</span>');
                	Materialize.toast($toastContent, 3000,'green');
                }
                
            }
        }
    }
    
    /**
     * Changes the item view to text input editing.
     */
    function editItem() {
        $(this).closest('li').addClass('editing').find('.edit').focus();
    }
    
    /**
     * Submits updated item title and changes it's view back from editing.
     */
    function updateItem(event) {
        var input = $(this);
        var item = input.closest('li');
        
        // Check that the item hasn't already been edited, because this method could
        // be called twice: once for hitting the enter key and once for the lost focus.
        if (item.hasClass('editing')) {

            // Submit changes if focused out of input or if enter was pressed.
            if (event.type === 'focusout' || event.which === KEY.ENTER) {
                var value = input.val().trim();

                item.removeClass('editing').find('label').text(value);

                // Only save if the value is not empty.
                if (value.length) {
                    performAction(input, value);
                // Remove the item if the value was left empty.
                } else {
                    item.find('.destroy').trigger('click');
                }

            // If escape was pressed, reset everything.
            } else if (event.which === KEY.ESCAPE) {
                item.removeClass('editing');
                input.val(item.find('label').text());
            }
        }
    }

    /**
     * Completes/reopens a todo item.
     */
    function toggleItem() {
        var input = $(this);
        
        performAction(input, input.is(':checked'));
    }
    
    function changeStatusItem(element, value) {
    	console.log('changeStatusItem');
    	try{
	    	performAction(element,value);
	    	var x = $.parseJSON(element.currentTarget.attributes['data-action'].value);
			if(x.data.status == 'done'){
				var $toastContent = $('<span>Well done!</span>');
            	Materialize.toast($toastContent, 3000,'blue');
           
			}
    	}
    	catch(erro){
    		var $toastContent = $('<span> script.js: Error line 344 - '+erro+'.</span>');
        	Materialize.toast($toastContent, 8000,'red');
    	}
    }
    
    // For the multiple ':applyTo' parameters to not be transformed into ':applyTo[]' upon POST, this has to be disabled
    $.ajaxSettings.traditional = true;

    // Attaching all events using delegation
    todoapp.on('keyup', '#new-todo', addItem);
    todoapp.on('dblclick', '.view label', editItem);
    todoapp.on('focusout keyup', '.edit', updateItem);
    todoapp.on('click', '.destroy', performAction);
    todoapp.on('change', '.toggle', toggleItem);
    todoapp.on('change', '#toggle-all', performAction);
    todoapp.on('click', '#clear-completed', performAction);
    
    todoapp.on('click','.status', changeStatusItem);
    
    doingTask.on('click', '.destroy', performAction);
    doingTask.on('click', '.status', changeStatusItem);
    

})(jQuery);
